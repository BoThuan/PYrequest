import time
from random import randint

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager


rand = randint(1,4)

# Turn off notifications-
options = Options()
options.add_argument("--disable-notifications")

# Webdriver-
driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=options)
# Browser window size-
driver.maximize_window()

# Link to website-
driver.get ("https://vi-vn.facebook.com")
time.sleep(rand)

# Find email id and enter email-
email = driver.find_element_by_id('email')
email.send_keys("**Username**")
time.sleep(rand)

# Find pass id and enter pass
passw = driver.find_element_by_id('pass')
passw.send_keys('**Pass**')
time.sleep(rand)

# Find button login và enter
btnLogin = driver.find_element_by_name('login')
btnLogin.send_keys(Keys.ENTER)
time.sleep(3)

#search friend
search_friend = driver.find_element_by_xpath("//input[@placeholder='Tìm kiếm trên Facebook']")
search_friend.send_keys('**name_friend**')
time.sleep(rand)
li_friend = driver.find_elements_by_xpath("//*[@class='k4urcfbm']")
li_friend[1].click()
time.sleep(20)

# Auto like post
print('START!!')
time.sleep(0.5)
for i in range(0, 6):
    likes = driver.find_elements_by_xpath("//div[@class='tvfksri0 ozuftl9m']//span[text() = 'Thích']")
    print(likes[i].text + str(i+1) + "Post")
    likes[i].click()
    time.sleep(5)
print("\n"+"Like Ending!!")
driver.quit()






