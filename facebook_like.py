import time

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.action_chains import ActionChains


# Turn off notifications-
options = Options()
options.add_argument("--disable-notifications")

# Webdriver-
driver = webdriver.Chrome(ChromeDriverManager().install(),chrome_options=options)
# Browser window size-
driver.maximize_window()

# Link to website-
driver.get ("https://vi-vn.facebook.com")
time.sleep(2)

# Find email id and enter email-
email = driver.find_element_by_id('email')
email.send_keys("bothuan24680@gmail.com")
time.sleep(2)

# Find pass id and enter pass
passw = driver.find_element_by_id('pass')
passw.send_keys("Th0186997")
time.sleep(2)

# Find name login và enter
btnLogin = driver.find_element_by_name('login')
btnLogin.send_keys(Keys.ENTER)
time.sleep(2)

# Scroll web page
driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
time.sleep(5)

# Xpath Like Post
# 1/ likes = driver.find_elements_by_xpath("//div[@class='tvfksri0 ozuftl9m']//div[@aria-label='Thích']")
# 2/ likes = driver.find_elements_by_xpath("//div[@class='tvfksri0 ozuftl9m']//span[text() = 'Thích']")

#Auto like post
for i in range(0, 6):
    likes = driver.find_elements_by_xpath("//div[@class='tvfksri0 ozuftl9m']//div[@aria-label='Thích']")
    actions = ActionChains(driver)
    actions.move_to_element(likes[i]).perform()
    likes[i].click()
    print(likes[i].text+ " " + str(i+1) +" POST")
    time.sleep(1)

driver.close()



